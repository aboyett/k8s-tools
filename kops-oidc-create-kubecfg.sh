#!/bin/bash
S3_STATE_STORE="$KOPS_STATE_STORE"
CLUSTER=$1
USERNAME=$2

# get the name of the ca cert that kops created in the 
# state store s3 bucket
cert=$(aws s3 ls $S3_STATE_STORE/$CLUSTER/pki/issued/ca/ | awk '{print $4}')

# copy the ca cert locally for kubectl to reference
aws s3 cp $S3_STATE_STORE/$CLUSTER/pki/issued/ca/"$cert" ~/.kube/"$cert"

# create a cluster in kubeconfig
kubectl config set-cluster $CLUSTER \
    --certificate-authority="$cert" \
    --server=https://api."$CLUSTER"

# create a context for the oidc user in the kubconfig
kubectl config set-context $USERNAME \
  --cluster $CLUSTER \
  --user $USERNAME
